
# InMoment Mobile SDK

This repository covers all content that is publicly available for consumption, that is provided by InMoment.

## Instructions and documents

* [InAppSurvey SDK Documentation (Native App)](https://inmoment-ios-artifacts.s3.amazonaws.com/InAppSurvey/Docs/1.0.0/index.html) 

* [Cordova Plugin Integration - (iOS and Android)](https://inmoment-ios-artifacts.s3.amazonaws.com/InAppSurvey/Docs/Cordova/index.html)



## Contacts

* **Janet May**- [jmay@inmoment.com](mailto:jmay@inmoment.com)
