package com.example.sdktestapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.maritzcx.inappsurvey.SurveyPresentation.InAppSurveyCallBack;
import com.maritzcx.inappsurvey.SurveyPresentation.Models.CustomPromptDataModel;
import com.maritzcx.inappsurvey.SurveyPresentation.Models.RuleRequestModel;
import com.maritzcx.inappsurvey.SurveyPresentation.Presenter;
import com.maritzcx.inappsurvey.SurveyPresentation.SurveyResultEnum;


import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements InAppSurveyCallBack {

    static String TAG = "Main Activity";

    static String programToken = "insert_your_program_token_value";
    static String feedbackEvent = "feedback";
    static String billPayEvent = "billpay";
    static String transferEvent = "transfer";
    static String VideoEvent = "videoTest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.feedbackBtn).setOnClickListener((view -> {

            String prePopJSON = "{\"USERNAME\":\"User\", \"PAGE_ID\":\"Feedback\"}";
            RuleRequestModel aRuleRequestModel = new RuleRequestModel(programToken, feedbackEvent, 1, null, true);
            Gson gson = new Gson();
            Map<String, String> map = new HashMap<>();
            map = gson.fromJson(prePopJSON, map.getClass());

            for (Map.Entry<String, String> entry : map.entrySet()) {
                Log.e("InAppSurvey", entry.getKey() + "=" + entry.getValue());
                aRuleRequestModel.addPrePopulationData(entry.getKey(), entry.getValue());
            }
            Presenter aPresenter = Presenter.getInstance();
            aPresenter.initiateSurvey(MainActivity.this, aRuleRequestModel);

        }));

        findViewById(R.id.transferBtn).setOnClickListener((view -> {

            String customPromptJson =
                    "{\"headerImageURL\":\"https://maritzcxenterpriseoperations.allegiancetech.com/surveys/images/DPNK87/Preview/cx2_hero_large.jpg\",\"bodyText\":\"Your feedback is incredibly valuable and will allow us to focus on those aspects that are most important to you. <br /><br /> Please take our 1 minute survey.\",\t\"bodyBackgroundColor\":\"#ffffff\",\"mainBackgroundColor\":\"#7122ff\", \"noButtonBackgroundColor\":\"#efb237\",\"noButtonText\":\"No Thanks\", \"noButtonTextColor\":\"#ffffff\",\"yesButtonBackgroundColor\":\"#efb237\", \"yesButtonText\":\"Start Survey\",\"yesButtonTextColor\":\"#ffffff\", \"footerBackgroundColor\":\"#0d55d0\",\"footerText\":\"Please contact if need more details.\", \"footerTextColor\":\"#ffffff\",\"footerHeaderText\":\"Thank you very much.\", \"optOutButtonText\":\"Opt-Out\", \"optOutButtonTextColor\":\"#efb237\", \"disclosureText\": \"If you decline this invitation, we will not offer this survey again while using this platform for a period of time.\"," +
                            "\"disclosureTextColor\":\"#000000\"}";

            String prePopJSON = "{\"USERNAME\":\"user\", \"PAGE_ID\":\"Transfer\"}";

            RuleRequestModel aRuleRequestModel = new RuleRequestModel(programToken, transferEvent, 1, null, true);

            CustomPromptDataModel customPromptDataModel = prepareCustomPromtDataModel(customPromptJson);

            Gson gson = new Gson();
            Map<String, String> map = new HashMap<>();
            map = gson.fromJson(prePopJSON, map.getClass());

            for (Map.Entry<String, String> entry : map.entrySet()) {
                Log.e("InAppSurvey", entry.getKey() + "=" + entry.getValue());
                aRuleRequestModel.addPrePopulationData(entry.getKey(), entry.getValue());
            }

            Presenter aPresenter = Presenter.getInstance();
            aPresenter.initiateSurvey(MainActivity.this, aRuleRequestModel,customPromptDataModel);

        }));


        findViewById(R.id.billpayBtn).setOnClickListener((view -> {

            RuleRequestModel aRuleRequestModel = new RuleRequestModel(programToken, billPayEvent, 1, null, true);

            String prePopJSON = "{\"USERNAME\":\"user\", \"PAGE_ID\":\"Bill Pay\"}";

            Gson gson = new Gson();
            Map<String, String> map = new HashMap<>();
            map = gson.fromJson(prePopJSON, map.getClass());

            for (Map.Entry<String, String> entry : map.entrySet()) {
                Log.e("InAppSurvey", entry.getKey() + "=" + entry.getValue());
                aRuleRequestModel.addPrePopulationData(entry.getKey(), entry.getValue());
            }

            Presenter aPresenter = Presenter.getInstance();
            aPresenter.initiateSurvey(MainActivity.this, aRuleRequestModel);


        }));

        findViewById(R.id.videoTestBtn).setOnClickListener((view -> {

            RuleRequestModel aRuleRequestModel = new RuleRequestModel(programToken, VideoEvent, 1, null, true);

            Presenter aPresenter = Presenter.getInstance();
            aPresenter.initiateSurvey(MainActivity.this, aRuleRequestModel);

        }));
    }

    private CustomPromptDataModel prepareCustomPromtDataModel(String jsonString) {
        if (jsonString != null) {
            Gson aGson = new Gson();
            return aGson.fromJson(jsonString, CustomPromptDataModel.class);
        }
        return null;
    }

    @Override
    public void onSurveyUpdate(SurveyResultEnum mSurveyResultEnum) {
        switch (mSurveyResultEnum) {
            case COMPLETED:
                Log.d(TAG, "Call Back " + "On Completed call");
                break;
            case CANCELLED:
                Log.d(TAG, "Call Back " + "Survey Cancelled");
                break;
            case IN_PROGRESS:
                Log.d(TAG, "Call Back " + "On Progress Call");
                break;
            case OPT_OUT:
                Log.d(TAG, "Call Back " + "On OPT Call");
                break;
            case NO_NEED_PRESENT:
                Log.d(TAG, "Call Back " + "On No Need Present Cal");
                break;
            case ERROR_OCCURRED:
                Log.d(TAG, "Call Back " + "On Error Occurred");
                break;
        }
    }
}