var rootOfTheServiceURLs = 'https://inappsurvey.maritzstage.com/InAppSurvey/InAppServices';
/***********************************************************************************************
Call this method to get the details of a survey and whether the survey should be presented
to the user.  The parameter mdonotcountasactivity is set to 'true' when you want to see if
the user should get a survey but, not necessarily have it count as an attempt to retrieve
the survey.

The method parameter explanations are;
    meventalias - The pre-defined event alias that is agreed upon between the business
    and IT for the hosting app.
    mprogramtoken - The pre-defined program token that is agreed upon between the business
    and IT for the hosting app.
    mdevicetoken - A unique identifier for the user's device.  This could depend upon the
    platform that the app is hosted in.
    mdonotcountasactivity - Sometimes you want to show that a survey is available and not
    count it as an attempt.  This flag allows you to indicated it.  The default is false
    so the attempt will be counted.

Create a method named DoneWithSurveyAttempt() to retrieve the results.  Here is a sample:
DoneWithSurveyAttempt(resultsofcall) {
    if (resultsofcall == null) {
        //error
    } else {
        var surveyTakePrompt = resultsofcall.surveyTakePrompt;
        var titleForSurveyTakePrompt = resultsofcall.titleForSurveyTakePrompt;
        var yesPrompt = resultsofcall.yesPrompt;
        var noPrompt = resultsofcall.noPrompt;
        var returnAPrompt = resultsofcall.returnAPrompt;
        var SurveyURL = resultsofcall.SurveyURL;
        var presentASurvey = resultsofcall.presentASurvey;
        var id = resultsofcall.id;
        var isSuccess = resultsofcall.isSuccess;
    }
}
if isSuccess is false, that means that an unreliable condition has occurred and it should
be reported.
***********************************************************************************************/

function CallRulesServiceToDisplaySurvey(meventalias, mprogramtoken, mdevicetoken, mdonotcountasactivity) {
    var theRequest = {
        donotcountasactivity: mdonotcountasactivity,
        eventalias: meventalias,
        locale: GetLocale(),
        programtoken: mprogramtoken,
        devicedetails: GetBrowserDetails(),
        devicetoken: mdevicetoken,
        devicetype: 2
    }

    $.ajax({
        url: rootOfTheServiceURLs + '/Rules',
        type: 'post',
        dataType: 'json',
        error: function(xhr, status, error) {
            DoneWithSurveyAttempt();
        },
        success: function (data) {
            DoneWithSurveyAttempt(data);
        },
        data: theRequest
    });
}

/***********************************************************************************************
Call this method to get the details to inform the system that the survey has been taken by
the user.  This is useful for rules where the user should never take a survey more than once.

The method parameter explanations are;
    mruleid - An internal identifier for a rule that has been executed to deliver a survey.
    After you call the method CallRulesServiceToDisplaySurvey(), you will get an id.  That
    is the ruleid that has been executed to generate a survey.  For tracking purposes, the
    below method should be called upon completion of that survey.  This allows for us to
    track who has and who hasn't taken a survey for survey presentation rules purposes.
    mdevicetoken - A unique identifier for the user's device.  This could depend upon the
    platform that the app is hosted in.

Create a method named DoneWithSurveySubmitAttempt() to retrieve the results.  Here is a sample:
DoneWithSurveySubmitAttempt(resultsofcall) {
    if (resultsofcall == null) {
        //error
    } else {
        var isSuccess = resultsofcall.isSuccess;
    }
}
***********************************************************************************************/
function CallSurveySubmittedService(mruleid, mdevicetoken) {

    var theRequest = {
        ruleID: mruleid,
        devicedetails: GetBrowserDetails(),
        devicetoken: mdevicetoken,
        devicetype: 2
    }

    $.ajax({
        url: rootOfTheServiceURLs + '/SubmittedSurvey',
        type: 'post',
        dataType: 'json',
        error: function(xhr, status, error) {
            DoneWithSurveySubmitAttempt();
        },
        success: function (data) {
            DoneWithSurveySubmitAttempt(data);
        },
        data: theRequest
    });
}

function GetBrowserDetails() {
    var theResult = "Cannot acquire ";
    if (typeof navigator.appVersion !== "undefined") {
    	theResult = navigator.appVersion;
        theResult = theResult.replace(/\;/g, "");
        theResult = theResult.replace(/\(/g, "");
        theResult = theResult.replace(/\)/g, "");
        theResult = theResult.replace(/\//g, "");
    }
    return theResult;
}

function GetLocale() {
    var theResult = "en_US";
    if (typeof navigator.language !== "undefined") {
    	theResult = navigator.language.replace("-","_");
    }

    return theResult;
}