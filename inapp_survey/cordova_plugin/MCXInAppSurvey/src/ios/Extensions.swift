//
//  UIColorExtension.swift
//

import Foundation

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension String {
	
	func stringFromHtml() -> NSAttributedString? {
		do {
			let data = self.data(using: String.Encoding.utf8, allowLossyConversion: true)
			if let data = data {
				let str = try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
				return str
			}
		} catch {
		}
		return nil
	}
	
	func prepareJSONObject() -> [String: Any]? {
		if let data = self.data(using: String.Encoding.utf8), data.count > 0 {
			do {
				if let response = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
					return response
				}
			} catch let parseError as NSError {
				MCXLogger.sharedLogger.log("JSONSerialization error :", parseError.localizedDescription)
			}
		}
		return nil
	}
}

extension UIButton {
	
	func setUnderlinedText(_ text: String, textColor: UIColor? = .black) {
		
		let font = self.titleLabel?.font ?? UIFont.systemFont(ofSize: 16.0)
		
		let attrs: [NSAttributedString.Key: Any] = [
			NSAttributedString.Key.font: font,
			NSAttributedString.Key.foregroundColor: textColor!,
			NSAttributedString.Key.underlineStyle: 1]
		let buttonTitleStr = NSMutableAttributedString(string: text, attributes:attrs)
		self.setAttributedTitle(buttonTitleStr, for: .normal)
	}
}
