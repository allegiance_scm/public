//
//  Models
//

import Foundation
import UIKit

struct RuleResponseModel: Codable {
	var id: Int?
	var surveyTakePrompt: String?
	var titleForSurveyTakePrompt: String?
	var yesPrompt: String?
	var noPrompt: String?
	var optOutPrompt: String?
	var returnAPrompt: Bool?
	var SurveyURL: String?
	var presentASurvey: Bool?
	var isSuccess: Bool?
	
    var isValidPrompt: Bool {
        if surveyTakePrompt != nil && yesPrompt != nil && noPrompt != nil && titleForSurveyTakePrompt != nil {
            return true
        }
        return false
    }
}

public struct CustomPromptModel: Codable {
	
    public var headerImageURL: String?
    public var bodyText: String?
    public var bodyBackgroundColor: String?
    public var mainBackgroundColor: String?
    public var noButtonBackgroundColor: String?
    public var noButtonText: String?
    public var noButtonTextColor: String?
    public var yesButtonBackgroundColor: String?
    public var yesButtonText: String?
    public var yesButtonTextColor: String?
    public var footerBackgroundColor: String?
    public var footerText: String?
    public var footerTextColor: String?
    public var footerHeaderText: String?
	public var optOutButtonText: String?
	public var optOutButtonTextColor: String?
	public var disclosureText: String?
	public var disclosureTextColor: String?
	
	var bodyBackgroundUIColor: UIColor {
		return (bodyBackgroundColor != nil) ? UIColor(hexString: bodyBackgroundColor!) : UIColor.white
	}
	
	var mainBackgroundUIColor: UIColor {
		return (mainBackgroundColor != nil) ? UIColor(hexString: mainBackgroundColor!) : UIColor.white
	}
	
	var noButtonBackgroundUIColor: UIColor {
		return (noButtonBackgroundColor != nil) ? UIColor(hexString: noButtonBackgroundColor!) : UIColor.white
	}
	
	var noButtonTextUIColor: UIColor {
		return (noButtonTextColor != nil) ? UIColor(hexString: noButtonTextColor!) : UIColor.black
	}
	
	var yesButtonBackgroundUIColor: UIColor {
		return (yesButtonBackgroundColor != nil) ? UIColor(hexString: yesButtonBackgroundColor!) : UIColor.white
	}
	
	var yesButtonTextUIColor: UIColor {
		return (yesButtonTextColor != nil) ? UIColor(hexString: yesButtonTextColor!) : UIColor.black
	}
	
	var footerBackgroundUIColor: UIColor {
		return (footerBackgroundColor != nil) ? UIColor(hexString: footerBackgroundColor!) : UIColor.white
	}
	
	var footerTextUIColor: UIColor {
		return (footerTextColor != nil) ? UIColor(hexString: footerTextColor!) : UIColor.black
	}
	
	var optOutButtonTextUIColor: UIColor {
		return (optOutButtonTextColor != nil) ? UIColor(hexString: optOutButtonTextColor!) : UIColor.black
	}
	
	var disclosureTextUIColor: UIColor {
		return (disclosureTextColor != nil) ? UIColor(hexString: disclosureTextColor!) : UIColor.black
	}
	
    //private init(){}
	
	public init?(json: [String: Any]) {
		
		guard let bodyTextValue = json["bodyText"] as? String, let yesButtonValue = json["yesButtonText"] as? String, let noButtonValue = json["noButtonText"] as? String else {
			return nil
		}
		
		headerImageURL = json["headerImageURL"] as? String
		bodyText = bodyTextValue
		bodyBackgroundColor = json["bodyBackgroundColor"] as? String
		mainBackgroundColor = json["mainBackgroundColor"] as? String
		noButtonBackgroundColor = json["noButtonBackgroundColor"] as? String
		noButtonText = noButtonValue
		noButtonTextColor = json["noButtonTextColor"] as? String
		yesButtonBackgroundColor = json["yesButtonBackgroundColor"] as? String
		yesButtonText = yesButtonValue
		yesButtonTextColor = json["yesButtonTextColor"] as? String
		footerBackgroundColor = json["footerBackgroundColor"] as? String
		footerText = json["footerText"] as? String
		footerTextColor = json["footerTextColor"] as? String
		footerHeaderText = json["footerHeaderText"] as? String
		optOutButtonText = json["optOutButtonText"] as? String
		optOutButtonTextColor = json["optOutButtonTextColor"] as? String
		disclosureText = json["disclosureText"] as? String
		disclosureTextColor = json["disclosureTextColor"] as? String
	
		if let optOut = optOutButtonText, !optOut.isEmpty {
			disclosureText = nil
			disclosureTextColor = nil
		}
	}
	
	mutating func validateModel() {
		if let optOut = optOutButtonText, !optOut.isEmpty {
			disclosureText = nil
			disclosureTextColor = nil
		}
	}
	
	public func isValid() -> Bool {
		
		if headerImageURL != nil && bodyText != nil && noButtonText != nil && yesButtonText != nil  {
			return true
		}
		return false
	}
}

public struct RuleRequestModel: Encodable {
//    public var localeobject: Locale?
    public var locale: String?
    public var programtoken: String?
    public var eventalias: String?
    public let requestcode = 1
    public let donotcountasactivity = false
    public var prepopulationdata = [PrePopDataModel]()
    public var devicetoken: String
    public let devicetype = 1
    public var devicedetails: String
    
    public init(programToken: String, eventName: String, prePopData: [PrePopDataModel]? = nil) {
        self.programtoken = programToken
        self.eventalias = eventName
        self.locale = Locale.current.languageCode! + "_" + Locale.current.regionCode!
		self.devicetoken =  UIDevice.current.identifierForVendor!.uuidString
        self.devicedetails = UIDevice.current.model + "|" + UIDevice.current.systemVersion
		if let prePopData = prePopData {
			self.prepopulationdata = prePopData
		}
    }
	
	public mutating func setLocale(_ localeObj: Locale) {
		locale = (localeObj.languageCode ?? "en") + "_" + (localeObj.regionCode ?? "US")
	}
	
	public mutating func addPrePopulationData(namedField: String, namedfieldvalue: String) {
		let prepopDataModel = PrePopDataModel(namedfield: namedField, namedfieldvalue: namedfieldvalue)
		self.prepopulationdata.append(prepopDataModel)
	}
}

public struct PrePopDataModel: Codable {
	var namedfield: String
	var namedfieldvalue: String
}

struct SubmittedSurveyRequestModel: Encodable {
	var ruleId: Int
	var devicedetails: String
	var devicetoken: String
	let devicetype = 1
	
	init(ruleId: Int) {
		self.ruleId = ruleId
		self.devicetoken =  UIDevice.current.identifierForVendor!.uuidString
		self.devicedetails = UIDevice.current.model + "|" + UIDevice.current.systemVersion
	}
}
