//
//  ActivityIndicator.swift
//

import UIKit

@objc class ActivityIndicator: NSObject {
    
    @objc func showActivityIndicator(viewController:UIViewController) -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        activityIndicator.center = viewController.view.center
        activityIndicator.hidesWhenStopped = true
		activityIndicator.style = .gray
        viewController.view.addSubview(activityIndicator)
        
        return activityIndicator
    }
	
	func activityIndicator() -> UIActivityIndicatorView {
		let activityIndicator = UIActivityIndicatorView()
		activityIndicator.hidesWhenStopped = true
		activityIndicator.style = .gray
		return activityIndicator
	}
}
