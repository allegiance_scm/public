//
//  MCXInAppSurvey.swift
//  
//
//

public enum SurveyResult: String {
	case completed = "Survey Completed"
	case cancelled = "Survey Cancelled"
	case inProgress = "Survey Presentation in progress"
	case optOut = "Opt-Out Survey"
	case errorOccured = "Error Occured"
}

@objc(MCXInAppSurvey) class MCXInAppSurvey : CDVPlugin, CordovaPluginDelegate {

	var pluginResult: CDVPluginResult!
	var cdvCommand: CDVInvokedUrlCommand!
	var aRuleResponse: RuleResponseModel?
	var ruleRequestModel: RuleRequestModel!
	var presenter: Presenter!
	var customPrompt: String?
	var isSurveyPresenting = false
	
	@objc(initiateSurvey:)
	func initiateSurvey(command: CDVInvokedUrlCommand) {
		
		self.commandDelegate.run {
			
			self.cdvCommand = command
			self.pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
			
			// Check for arguments counts.
			guard command.arguments.count > 0 else {
				MCXLogger.sharedLogger.log("Did not pass Program Token nor Event Name")
				self.pluginError(message: "Did not pass Program Token nor Event Name")
				return
			}
			
			guard let input = command.arguments[0] as? [String: Any] else {
				MCXLogger.sharedLogger.log("Invalid Input")
				self.pluginError(message: "Invalid Input")
				return
			}
			
			guard let programToken = input["programToken"] as? String else {
				MCXLogger.sharedLogger.log("Did not pass Program Token")
				self.pluginError(message: "Did not pass Program Token")
				return
			}
			
			guard let eventName = input["eventName"] as? String else {
				MCXLogger.sharedLogger.log("Did not pass Event Name")
				self.pluginError(message: "Did not pass Event Name")
				return
			}
			
			if self.isSurveyPresenting {
				self.pluginError(message: SurveyResult.inProgress.rawValue)
				return
			}
			
			if let isDebug = input["debug"] as? Bool {
				MCXLogger.sharedLogger.isDebug = isDebug
			}
			
			self.customPrompt = input["customPrompt"] as? String
			
			let prePopData = input["prePopData"] as? String ?? ""
			
			let prePopDataModels = self.preparePrePopData(prePopData)
			
			self.ruleRequestModel = RuleRequestModel(programToken: programToken, eventName: eventName, prePopData: prePopDataModels)
			
			self.getRules()
		}
	}
	
	private func getRules() {
		
		isSurveyPresenting = true
		Services().getRules(rulesRequestModel: self.ruleRequestModel) { ruleResponseModel, error in
			
			DispatchQueue.main.async {
				
				if let responseModel = ruleResponseModel {
					self.prepareForSurvey(responseModel: responseModel)
				} else if let error = error {
					self.pluginError(message: error.localizedDescription)
				} else {
					MCXLogger.sharedLogger.log("Internal Server error in get rule API")
					self.pluginError(message: "Failed to initialize survey")
				}
				self.isSurveyPresenting = false
			}
		}
	}
	
	private func prepareForSurvey(responseModel: RuleResponseModel) {
		
		if responseModel.presentASurvey == true {
			// Present Survey.
			if responseModel.SurveyURL != nil {
				aRuleResponse = responseModel
				setRulesResult(responseModel)
			} else {
				self.pluginError(message: "Failed to initialize survey")
				MCXLogger.sharedLogger.log("SurveyURL not present")
			}
		} else {
			pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "Success")
			commandDelegate?.send(pluginResult, callbackId: cdvCommand.callbackId)
		}
	}
	
	private func pluginError(message: String) {
		pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: message)
		commandDelegate?.send(pluginResult, callbackId: cdvCommand.callbackId)
	}

	private func setRulesResult(_ responseModel: RuleResponseModel) {
		
        presenter = Presenter(ruleRequest: ruleRequestModel, ruleResponse: responseModel, currentController: viewController)
		if responseModel.returnAPrompt == true {
            let customPromptModel = prepareCustomPromptModel()
            // If we have to show default prompt and is not valid then we directly present the survey.
            if customPromptModel == nil && responseModel.isValidPrompt == false {
                presentSurvey()
            } else {
                presenter.displayPresentation(customPromptModel: customPromptModel, delegate: self)
                customPrompt = nil
            }
		} else {
			presentSurvey()
		}
	}
	
	private func removePresentedController() {
		if let controller = self.viewController.presentedViewController {
			controller.dismiss(animated: false, completion: nil)
		}
	}

	func cancelSurvey() {
		removePresentedController()
		surveyDidFinished(result: .cancelled)
	}

	func presentSurvey() {
		removePresentedController()
		presenter.displaySurvey()
	}
	
	func optOutSurvey() {
		removePresentedController()
		presenter.optOutProceed()
		surveyDidFinished(result: .optOut)
	}

	private func prepareCustomPromptModel() -> CustomPromptModel? {
		if let customPromptValue = customPrompt, let jsonResponse = customPromptValue.prepareJSONObject() {
			let customPromptModel = CustomPromptModel(json: jsonResponse)
			return customPromptModel
		}
		return nil
	}

	private func preparePrePopData(_ prePopValue: String) -> [PrePopDataModel]? {
		if let jsonResponse = prePopValue.prepareJSONObject() as? [String: String] {
			var prePopDataModels = [PrePopDataModel]()
			for (key, value) in jsonResponse {
				let model = PrePopDataModel(namedfield: key, namedfieldvalue: value)
				prePopDataModels.append(model)
			}
			return prePopDataModels
		}
		return nil
	}
	
	func surveyDidFinished(result: SurveyResult) {
		MCXLogger.sharedLogger.log(result)
		pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: result.rawValue)
		commandDelegate?.send(pluginResult, callbackId: cdvCommand.callbackId)
	}
}
