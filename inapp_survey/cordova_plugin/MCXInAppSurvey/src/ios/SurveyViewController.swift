//
//  SurveyViewController.swift
//

import Foundation
import WebKit

class SurveyViewController: UIViewController {
	
	var nextController: String = ""
	var ruleResponse: RuleResponseModel?
	weak var inAppSurveyDelegate: MCXInAppSurvey?
	var surveyUrl: String = ""
	private var actInd: UIActivityIndicatorView?
	private var isPresentedSurvey = false
	var surveyWebView : WKWebView!
	
	//MARK: - View Lifecycle
	
	override func loadView() {
		surveyWebView = WKWebView()
		surveyWebView.uiDelegate = self
		surveyWebView.navigationDelegate = self
		view = surveyWebView
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = .white
		automaticallyAdjustsScrollViewInsets = false
		
		self.title = nil
		DispatchQueue.main.async {
			self.loadSurveyWebView()
			self.actInd = ActivityIndicator().showActivityIndicator(viewController: self)
			self.navigationController?.navigationBar.tintColor = UIColor(hexString: "#B94700")
			self.addNavigationButton()
		}
	}
	
	private func loadSurveyWebView() {
		let url = URL(string: surveyUrl)!
		surveyWebView.load(URLRequest(url: url))
		surveyWebView.allowsBackForwardNavigationGestures = true
	}
	
	//MARK: - Private Methods
	private func addNavigationButton() {
		let bundle = Bundle(for: SurveyViewController.self)
		let image = UIImage(named: "mcx-close-window", in: bundle, compatibleWith: nil)
		let closeButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(closeButtonClicked))
		navigationItem.rightBarButtonItem = closeButton
	}

	private func displayExternalLinks(_ url: URL) {
		let surveyVC = SurveyViewController()
		surveyVC.surveyUrl = url.absoluteString
		surveyVC.isPresentedSurvey = true
		surveyVC.ruleResponse = ruleResponse
		let navigationVC = UINavigationController(rootViewController: surveyVC)
		navigationVC.modalPresentationStyle = .fullScreen
		present(navigationVC, animated: true, completion: nil)
	}
	
	@objc func closeButtonClicked() {
		
		if !isPresentedSurvey {
			inAppSurveyDelegate?.surveyDidFinished(result: SurveyResult.cancelled)
		}
		dismiss(animated: true, completion: nil)
	}
	
	private func handleCall(functionName:String, args:[URLQueryItem]?) {
		//Check for nil args value
		if (functionName == "submit") {
			handleSubmitSurvey()
			self.surveySubmitted()
		}
	}
	
	private func handleSubmitSurvey() {
		
		if let ruleResponseModel = ruleResponse, let ruleId = ruleResponseModel.id {
			let submittedRequest = SubmittedSurveyRequestModel(ruleId: ruleId)
			Services().submittedSurvey(requestModel: submittedRequest)
		}
	}
	
	private func surveySubmitted() {
		dismiss(animated: true) {
			self.inAppSurveyDelegate?.surveyDidFinished(result: SurveyResult.completed)
		}
	}
}

//MARK: - WKUIDelegate
extension SurveyViewController: WKUIDelegate {
	
	func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
		MCXLogger.sharedLogger.log("createWebViewWith navigationAction = ", navigationAction.request.url ?? "No URL")
		if let url = navigationAction.request.url, navigationAction.targetFrame == nil, !isPresentedSurvey {
			MCXLogger.sharedLogger.log("displayExternalLinks")
			displayExternalLinks(url)
		} else if isPresentedSurvey {
			MCXLogger.sharedLogger.log("Load url in same view")
			surveyWebView.load(navigationAction.request)
		}
		return nil
	}
	
	func webViewDidClose(_ webView: WKWebView) {
		
	}
	
	func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
		
		let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
			completionHandler()
		}))
		present(alertController, animated: true, completion: nil)
	}
	
	
	func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
		
		let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		
		alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
			completionHandler(true)
		}))
		
		alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
			completionHandler(false)
		}))
		present(alertController, animated: true, completion: nil)
	}
	
	
	func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
		
		let alertController = UIAlertController(title: nil, message: prompt, preferredStyle: .alert)
		
		alertController.addTextField { (textField) in
			textField.text = defaultText
		}
		alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
			if let text = alertController.textFields?.first?.text {
				completionHandler(text)
			} else {
				completionHandler(defaultText)
			}
		}))
		
		alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
			completionHandler(nil)
		}))
		present(alertController, animated: true, completion: nil)
	}
}

extension SurveyViewController: WKNavigationDelegate {
	
	func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
		
		guard let url = navigationAction.request.url else {
			decisionHandler(.allow)
			return
		}
		
		MCXLogger.sharedLogger.log("decidePolicyFor URL = ", url)
		
		// Handle phone and email links
		if url.scheme == "tel" || url.scheme == "mailto" {
			if UIApplication.shared.canOpenURL(url) {
				UIApplication.shared.openURL(url)
				decisionHandler(.cancel)
				return
			}
		}
		
		let components = URLComponents(url: url, resolvingAgainstBaseURL: false)
		
		if (components?.host?.lowercased() == "mcxmobileappsurvey") {
			let path = components?.path
			let args = components?.queryItems
			let pathSegments = path?.components(separatedBy: "/")
			let function = pathSegments?[1]
			self.handleCall(functionName: function!, args: args)
			decisionHandler(.cancel)
			return
		}
		
		decisionHandler(.allow)
	}
	
	func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Swift.Void) {
		decisionHandler(.allow)
	}
	
	private func handleLoadingError(_ error: Error) {
		let errorCode = (error as NSError).code
		switch errorCode {
		case -999:
			actInd?.stopAnimating()
		case 102:
			break
		default:
			actInd?.stopAnimating()
		}
	}
	
	func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
		actInd?.startAnimating()
	}
	
	func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
		MCXLogger.sharedLogger.log(#function, error)
		handleLoadingError(error)
	}
	
	func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
		MCXLogger.sharedLogger.log(#function, error)
		handleLoadingError(error)
	}
	
	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
		actInd?.stopAnimating()
	}
}
