//
//  Presenter.swift
//

import Foundation

class Presenter: UIView {
	
    var ruleResponse: RuleResponseModel
	var ruleRequestModel: RuleRequestModel

	weak var currentController: UIViewController?
	weak var surveyDelegate: MCXInAppSurvey?

	init(ruleRequest: RuleRequestModel, ruleResponse: RuleResponseModel, currentController: UIViewController) {
		self.ruleRequestModel = ruleRequest
        self.ruleResponse = ruleResponse
		self.currentController = currentController
		super.init(frame: CGRect.zero)
    }
	
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

	func displayPresentation(customPromptModel: CustomPromptModel?, delegate: MCXInAppSurvey) {
		
		surveyDelegate = delegate
		
		if let customPromptModel = customPromptModel {
			// Custom Prompt configuration here.
			let bundle = Bundle(for: Presenter.self)
			let storyboard = UIStoryboard(name: "MCXSurvey", bundle: bundle)
			if let customPrompt = storyboard.instantiateInitialViewController() as? CustomPromptController {
				customPrompt.promptModel = customPromptModel
				customPrompt.cordovaDelegate = delegate
				customPrompt.modalPresentationStyle = .fullScreen
				currentController?.present(customPrompt, animated: true, completion: nil)
			}
		} else {
			// Default Prompt Configuration here.
			configureDefaultPrompt()
		}
	}
	
	private func configureDefaultPrompt() {
		
		let yesAction = UIAlertAction(title: ruleResponse.yesPrompt, style: .default) { (action) -> Void in
			self.displaySurvey()
		}
		
		let noAction = UIAlertAction(title: ruleResponse.noPrompt, style: .default) { (action) -> Void in
			self.surveyDelegate?.surveyDidFinished(result: .cancelled)
		}
		
		let alertViewController = UIAlertController(title: ruleResponse.titleForSurveyTakePrompt, message: ruleResponse.surveyTakePrompt, preferredStyle: .alert)
		
		var actions = [UIAlertAction]()
		
		if let optOutPrompt = ruleResponse.optOutPrompt, optOutPrompt.count > 0 {
			let optOutAction = UIAlertAction(title: optOutPrompt, style: .default) { (action) -> Void in
				MCXLogger.sharedLogger.log("opt out button clicked")
				self.optOutProceed()
			}
			actions.append(yesAction)
			actions.append(noAction)
			actions.append(optOutAction)
		} else {
			actions.append(noAction)
			actions.append(yesAction)
		}
		
		for action in actions {
			alertViewController.addAction(action)
		}
		
		currentController?.present(alertViewController, animated: true,	completion: nil)
	}
	
	func optOutProceed() {
		Services().optOut(rulesRequestModel: ruleRequestModel)
	}
	
	func displaySurvey() {
		let surveyView = SurveyViewController()
		surveyView.surveyUrl = ruleResponse.SurveyURL!
		surveyView.ruleResponse = ruleResponse
		surveyView.inAppSurveyDelegate = surveyDelegate
		
		let navigationVC = UINavigationController(rootViewController: surveyView)
		navigationVC.modalPresentationStyle = .fullScreen
		currentController?.present(navigationVC, animated: true, completion: nil)
	}
}
