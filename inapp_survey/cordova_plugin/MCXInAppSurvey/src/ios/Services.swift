//
//  Services.swift
//

import Foundation

class Services {
    
    let ruleUrl = "https://inappsurvey.mcxtools.com/InAppSurvey/InAppServices/Rules"
    let optOutUrl = "https://inappsurvey.mcxtools.com/InAppSurvey/InAppServices/OptOut"
    let submittedSurveyUrl = "https://inappsurvey.mcxtools.com/InAppSurvey/InAppServices/SubmittedSurvey"
    
    typealias rulesResults = (RuleResponseModel?, Error?) -> ()
    
    func getRules(rulesRequestModel: RuleRequestModel, completion: @escaping rulesResults) {
        let myURL = URL(string: ruleUrl)
        MCXLogger.sharedLogger.log("URL = ", ruleUrl)
        var request = URLRequest(url:myURL!)
        request.httpMethod = "POST"
        do {
            let jsonData = try JSONEncoder().encode(rulesRequestModel)
			request.httpBody = jsonData
			
			// For debugging only
			if let postBody = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
				MCXLogger.sharedLogger.log("\nPost Body of getRules = ", postBody)
			}
			//
			
        } catch {
            MCXLogger.sharedLogger.log("\nPost Body of getRules error = ", error)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: request) {
            (data: Data?, response: URLResponse?, error: Error?) in
			
			if let responseData = data {
				let results = self.updateRules(responseData)
				completion(results.0, results.1)
			}
            if error != nil {
				MCXLogger.sharedLogger.log("\nError = ", error ?? "No Error")
				completion(nil, error)
            }
        }
        task.resume()
    }
    
    fileprivate func updateRules(_ data: Data) -> (RuleResponseModel?, Error?) {
		
		do {
			let retVal = try JSONDecoder().decode(RuleResponseModel.self, from: data)
			MCXLogger.sharedLogger.log("\nResponse = ", retVal)
			return (retVal, nil)
		}
		catch let parseError as NSError {
			MCXLogger.sharedLogger.log("Update Rules error = ", parseError.localizedDescription)
			return (nil, parseError)
		}
    }
	
	func optOut(rulesRequestModel: RuleRequestModel) {
		
		let myURL = URL(string: optOutUrl)
        MCXLogger.sharedLogger.log("URL = ", optOutUrl)
		var request = URLRequest(url: myURL!)
		request.httpMethod = "POST"
		
		do {
			let jsonData = try JSONEncoder().encode(rulesRequestModel)
			request.httpBody = jsonData
			
			// For debugging only
			if let postBody = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
				MCXLogger.sharedLogger.log("\nPost Body for optOut = ", postBody)
			}
			//
		} catch {
			MCXLogger.sharedLogger.log("\nPost Body Errro for optOut = ", error)
		}
		
		request.addValue("application/json", forHTTPHeaderField: "Content-Type")
		
		let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
			
			if let responseData = data {
				do {
					if let jsonData = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any] {
						MCXLogger.sharedLogger.log("\noptOut Response = ", jsonData)
					}
				}
				catch {
					MCXLogger.sharedLogger.log("\nJSONSerialization Error = ", error)
				}
			} else {
				MCXLogger.sharedLogger.log("\nError = ", error ?? "No Error")
			}
		}
		task.resume()
	}
	
	
	func submittedSurvey(requestModel: SubmittedSurveyRequestModel) {
		
		let myURL = URL(string: submittedSurveyUrl)
        MCXLogger.sharedLogger.log("URL = ", submittedSurveyUrl)
		var request = URLRequest(url: myURL!)
		request.httpMethod = "POST"
		
		do {
			let jsonData = try JSONEncoder().encode(requestModel)
			request.httpBody = jsonData
			
			// For debugging only
			if let postBody = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
				MCXLogger.sharedLogger.log("\nPost Body for submittedSurvey = ", postBody)
			}
			//
		} catch {
			MCXLogger.sharedLogger.log("\nPost Body error submittedSurvey = ", error)
		}
		
		request.addValue("application/json", forHTTPHeaderField: "Content-Type")
		
		let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
			
			if let responseData = data {
				do {
					if let jsonData = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any] {
						MCXLogger.sharedLogger.log("\n submittedSurvey Response = ", jsonData)
					}
				}
				catch {
					MCXLogger.sharedLogger.log("\n JSONSerialization Error = ", error)
				}
			} else {
				MCXLogger.sharedLogger.log("\nError = ", error ?? "No Error")
			}
		}
		task.resume()
	}
}


class MCXLogger: NSObject {
	
	static let sharedLogger = MCXLogger()
	
	var isDebug = false
	
	private override init() { }
	
	func log(_ items: Any...) {
		if isDebug {
			print(items)
		}
	}
}
