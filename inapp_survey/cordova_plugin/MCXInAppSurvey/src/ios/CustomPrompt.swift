//
//  CustomPrompt.swift
//

import Foundation

class CustomPromptController: UIViewController {
	
	@IBOutlet weak var headerImage: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var bodyView: UIView!
	@IBOutlet weak var footerView: UIView!
	@IBOutlet weak var footerHeaderLabel: UILabel!
	@IBOutlet weak var footerTextLabel: UILabel!
	@IBOutlet weak var noButton: UIButton!
	@IBOutlet weak var yesButton: UIButton!
	@IBOutlet weak var optOutButton: UIButton!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var disclosureTextLabel: UILabel!

	var promptModel: CustomPromptModel!

	weak var cordovaDelegate: CordovaPluginDelegate?

	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor =  promptModel.mainBackgroundUIColor
		
		bodyView.backgroundColor = promptModel.bodyBackgroundUIColor
		
		noButton.backgroundColor = promptModel.noButtonBackgroundUIColor
		noButton.setTitle(promptModel.noButtonText, for: .normal)
		noButton.setTitleColor(promptModel.noButtonTextUIColor, for: .normal)
		
		yesButton.backgroundColor = promptModel.yesButtonBackgroundUIColor
		yesButton.setTitle(promptModel.yesButtonText, for: .normal)
		yesButton.setTitleColor(promptModel.yesButtonTextUIColor, for: .normal)
		
		titleLabel.attributedText = (promptModel.bodyText ?? "").stringFromHtml()
		titleLabel.font = UIFont(name: titleLabel.font.fontName, size: 20)
		
		footerView.backgroundColor = promptModel.footerBackgroundUIColor
		
		footerHeaderLabel.attributedText = (promptModel.footerHeaderText ?? "").stringFromHtml()
		footerHeaderLabel.textColor = promptModel.footerTextUIColor
		footerHeaderLabel.font = UIFont(name: footerHeaderLabel.font.fontName, size: 20)
		
		footerTextLabel.attributedText = (promptModel.footerText ?? "").stringFromHtml()
		footerTextLabel.textColor = promptModel.footerTextUIColor
		footerTextLabel.font = UIFont(name: footerTextLabel.font.fontName, size: 20)
		
		disclosureTextLabel.text = promptModel.disclosureText
		disclosureTextLabel.textColor = promptModel.disclosureTextUIColor
		disclosureTextLabel.isHidden = (promptModel.disclosureText ?? "").isEmpty
		
		if let optOutText = promptModel.optOutButtonText, !optOutText.isEmpty {
			optOutButton.isHidden = false
			optOutButton.setUnderlinedText(optOutText, textColor: promptModel.optOutButtonTextUIColor)
		} else {
			optOutButton.isHidden = true
		}
		
		if let headerImageUrlString = promptModel.headerImageURL, let headerImageUrl = URL(string: headerImageUrlString) {
			downloadImage(with: headerImageUrl)
		}
	}
	
	private func downloadImage(with url: URL) {
		
		activityIndicator.startAnimating()
		
		let session = URLSession(configuration: .default)
		
		let downloadImageTask = session.dataTask(with: url) { [weak self] (data, response, error) in
			
			guard let strongSelf = self else {
				return
			}
			DispatchQueue.main.async {
				strongSelf.activityIndicator.stopAnimating()
				if let imageData = data, let image = UIImage(data: imageData) {
					strongSelf.headerImage.image = image
				} else if let e = error {
					MCXLogger.sharedLogger.log("Image Download Error Occurred: \(e)")
				} else {
					MCXLogger.sharedLogger.log("Image Download - No response from server")
				}
			}
		}
		downloadImageTask.resume()
	}
	
	@IBAction func noButtonClicked(_ sender: UIButton) {
		MCXLogger.sharedLogger.log("noButton clicked")
		cordovaDelegate?.cancelSurvey()
	}
	
	@IBAction func yesButtonClicked(_ sender: UIButton) {
		MCXLogger.sharedLogger.log("yesButton clicked")
		cordovaDelegate?.presentSurvey()
	}
	
	@IBAction func optOutButtonClicked(_ sender: UIButton) {
		MCXLogger.sharedLogger.log("optOutButton clicked")
		cordovaDelegate?.optOutSurvey()
	}
	
}
