//
//  Delegates.swift
//

import Foundation

protocol CordovaPluginDelegate: AnyObject {
    func cancelSurvey()
    func presentSurvey()
	func optOutSurvey()
}
