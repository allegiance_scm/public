package cordova_plugin_mcx_inappsurvey;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

import com.maritzcx.inappsurvey.SurveyPresentation.Models.CustomPromptDataModel;
import com.maritzcx.inappsurvey.SurveyPresentation.Models.RuleRequestModel;
import com.maritzcx.inappsurvey.SurveyPresentation.Presenter;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import com.google.gson.Gson;

/**
 * This class echoes a string called from JavaScript.
 */
public class MCXInAppSurvey extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("initiateSurvey")) {

            String aProgramToken = null;
            String anEventAlias = null;
            CustomPromptDataModel customPromptDataModel = null;
            String prePopData = null;
            boolean isDebug = false;

            if (args.length() < 1) {
                Log.e("InAppSurvey","Did not pass Program Token nor Event Name");
                callbackContext.error("Did not pass Program Token nor Event Name");
                return false;
            }

            JSONObject input = args.getJSONObject(0);
            if (input == null) {
                Log.e("InAppSurvey","Did not pass Program Token nor Event Name");
                callbackContext.error("Did not pass Program Token nor Event Name");
                return false;
            }


            try {
                if (input.getString("programToken") != null) {
                    aProgramToken = input.getString("programToken");
                } else {
                    Log.e("InAppSurvey", "Did not pass Program Token");
                    callbackContext.error("Did not pass Program Token");
                    return false;
                }
            } catch (Exception e) {
                Log.e("InAppSurvey", "Did not pass Program Token");
                callbackContext.error("Did not pass Program Token");
                return false;
            }

            try {
                if (input.getString("eventName") != null) {
                    anEventAlias = input.getString("eventName");
                } else {
                    Log.e("InAppSurvey", "Did not pass Event Name");
                    callbackContext.error("Did not pass Event Name");
                    return false;
                }
            } catch (Exception e) {
                Log.e("InAppSurvey", "Did not pass Event Name");
                callbackContext.error("Did not pass Event Name");
                return false;
            }

            try {
                if (input.getString("customPrompt") != null) {
                    String customPrompt = input.getString("customPrompt");
                    customPromptDataModel = prepareCustomPromtDataModel(customPrompt);

                } else {
                    Log.e("InAppSurvey", "Did not pass customPrompt");
                }
            } catch (Exception e) {
                Log.e("InAppSurvey", "Did not pass customPrompt");
            }

            try {
                if (input.getString("prePopData") != null) {
                    prePopData = input.getString("prePopData");
                } else {
                    Log.e("InAppSurvey", "Did not pass prePopData");
                }
            }
            catch (Exception e) {
                Log.e("InAppSurvey", "Did not pass prePopData");
            }

            try {
                if (input.getBoolean("debug")) {
                    isDebug = input.getBoolean("debug");
					Log.e("InAppSurvey", "Debug mode");
                } else {
                    Log.e("InAppSurvey", "No Debug");
                }
            }
            catch (Exception e) {
                Log.e("InAppSurvey", "No Debug");
            }


            Log.e("InAppSurvey","Parameters included pass basic validation");

            RuleRequestModel aRuleRequestModel = new RuleRequestModel(aProgramToken, anEventAlias, 1, null, isDebug);

            aRuleRequestModel.setLocale(this.cordova.getActivity());


            if (prePopData != null) {

                Gson gson = new Gson();
                Map<String,String> map = new HashMap<String,String>();
                map = (Map<String,String>) gson.fromJson(prePopData, map.getClass());

                for (Map.Entry<String, String> entry : map.entrySet())
                {
                    //Log.e("InAppSurvey",entry.getKey() + "=" + entry.getValue());
                    aRuleRequestModel.addPrePopulationData(entry.getKey(), entry.getValue());
                }
            }

            this.initiateSurvey(aRuleRequestModel, customPromptDataModel ,callbackContext);
            return true;
        }
        return false;
    }


    private CustomPromptDataModel prepareCustomPromtDataModel(String jsonString) {
        if (jsonString != null) {
            Gson aGson = new Gson();
            CustomPromptDataModel customPromptDataModel = aGson.fromJson(jsonString, CustomPromptDataModel.class);
            return customPromptDataModel;
        }
        return null;
    }

    private void initiateSurvey(RuleRequestModel aRuleRequestModel, CustomPromptDataModel customPromptDataModel, CallbackContext callbackContext) {
        Presenter aPresenter = Presenter.getInstance();
        aPresenter.initiateSurvey(this.cordova.getActivity(), aRuleRequestModel, customPromptDataModel);
        callbackContext.success("SUCCESS");
    }
    //}
}
