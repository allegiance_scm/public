//
//  ViewController.swift
//  SDKSampleApp
//
//  Created by Vijay Radake on 25/01/21.
//

import UIKit
import InAppFramework

struct SDK
{
    private init() { }
    static let programToken     = "ShutterflyProgram"//"Demo_APAC_SDK_App"//"SodexoDIY_20220501"
    static let feedbackEvent    = "feedback"
    static let transactionEvent = "transaction" //"logged_in"//"<Transaction_Active_Event_Name>"
}

class ViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // 1. To enable debugging set true by default it is false. You can add this line in AppDelegate.swift file.
        MCXInAppSurvey.shared.loggingEnabled = true
    }
    
    
    /// This method to validate program token and events, Use only in sample app, not needed once you have program token and events.
    private func validateSDKConfiguration()
    {
        assert(SDK.programToken != "<Program_Token>", "Inalid Program Token")
        assert(SDK.feedbackEvent != "<Feedback_Passive_Event_Name>", "Invalid Event Name")
        //assert(SDK.transactionEvent != "<Transaction_Active_Event_Name>", "Invalid Event Name")
    }

    
    @IBAction func feedbackButtonClicked()
    {
        validateSDKConfiguration()
        
        // 2. Create RuleRequestModel instance using program token and event name.
        var requestModel = RuleRequestModel(programToken: SDK.programToken, eventName: SDK.feedbackEvent)
        
        // 3. Optionally you can add prepop data.
        //requestModel.addPrePopulationData(nameField: "RandomName", nameFieldValue: "Nigel")
       // requestModel.addPrePopulationData(nameField: "Field2", nameFieldValue: "Value2")

        // 4. Optionally you can set delegate to get survey result back.
        MCXInAppSurvey.shared.surveyDelegate = self
        
        // 5. Invoke survey call.
        // Here if you want custom prompt then pass custom prompt instance to this call. Refer documentation or customPrompt() for how to create custom prompt instance. Here if we doesn't pass CustomPromptModel instance then SDK will display default prompt or directly display survey depends on Event configuration on Admin site.
        MCXInAppSurvey.shared.initiateSurvey(controller: self, ruleRequestModel: requestModel)
    }
    
    @IBAction func transactionButtonClicked()
    {
        validateSDKConfiguration()
        
        // 2. Create RuleRequestModel instance using program token and event name.
        var requestModel = RuleRequestModel(programToken: SDK.programToken, eventName: SDK.transactionEvent)
        
        // 3. Optionally you can add prepop data.
        //requestModel.addPrePopulationData(nameField: "Field1", nameFieldValue: "Value1")
        //requestModel.addPrePopulationData(nameField: "Field2", nameFieldValue: "Value2")

        // 4. Optionally you can set delegate to get survey result back.
        MCXInAppSurvey.shared.surveyDelegate = self
        
        // 5. Invoke survey call.
        // Here if you want custom prompt then pass custom prompt instance to this call. CustomPromptModel instance is prepared in customPrompt().
        MCXInAppSurvey.shared.initiateSurvey(controller: self, ruleRequestModel: requestModel, customPromptModel: customPrompt())
    }
}

extension UIViewController
{
    
    /// This function create and configure custom prompt model.
    /// - Returns: An instance of CustomPromptModel.
    func customPrompt() -> CustomPromptModel {
        
        var customPrompt = CustomPromptModel()
        customPrompt.headerImageURL = "https://maritzcxenterpriseoperations.allegiancetech.com/surveys/images/DPNK87/Preview/cx2_hero_large.jpg"
        customPrompt.bodyText = "Your feedback is incredibly valuable and will allow us to focus on those aspects that are most important to you. <br /><br /> Please take our 1 minute survey."
        customPrompt.bodyBackgroundColor = "#ffffff"
        customPrompt.mainBackgroundColor = "#ffffff"
        customPrompt.noButtonBackgroundColor = "#f05323"
        customPrompt.noButtonText = "No Thanks"
        customPrompt.noButtonTextColor = "#ffffff"
        customPrompt.yesButtonBackgroundColor = "#f05323"
        customPrompt.yesButtonText = "Start Survey"
        customPrompt.yesButtonTextColor = "#ffffff"
        customPrompt.footerBackgroundColor = "#0d55d0"
        customPrompt.footerText = "Please contact if need more details."
        customPrompt.footerTextColor = "#ffffff"
        customPrompt.footerHeaderText = "Thank you very much."
        customPrompt.disclosureText = "If you decline this invitation, we will not offer this survey again while using this platform for a period of time."
        customPrompt.disclosureTextColor = "#000000"
        return customPrompt
    }
}

extension ViewController: SurveyResultDelegate
{
    /// Call back method, called when user cancel, complete survey. If any error occured it will return error message.
    /// - Parameters:
    ///   - result: Bool value for surevy result.
    ///   - errorMessage: Error message string if any error occured, nil in case of success.
    func surveyDidFinished(_ result: Bool, errorMessage: String?)
    {
        print("Survey did finished with result : ", result)
        if let errorMessage = errorMessage {
            print("Survey did finished with error : ", errorMessage)
        }
    }
}
