
# InMoment Mobile SDK Sample App.

This repository contains sample app which shows how to integrate and use InAppFramework / SDK in your application.

## How to use:

In sample, you can see below code snippet in ViewController.swift

	struct SDK
	 {
    	private init() { }
   		static let programToken     = "<Program_Token>"
    	static let feedbackEvent    = "<Feedback_Passive_Event_Name>"
   		static let transactionEvent = "<Transaction_Active_Event_Name>"
	 }


 Replace ProgramToken and EventName with the one which will be provided by InMoment team.
 

## Documents

* [Overview](https://bitbucket.org/allegiance_scm/public/src/master/inapp_survey/Documents/Overview.pdf)

* [Native App Integration - iOS](https://bitbucket.org/allegiance_scm/public/src/master/inapp_survey/Documents/Native_App_Integration_iOS.pdf)



## Contacts

* **Janet May**- [jmay@inmoment.com](mailto:jmay@inmoment.com)
